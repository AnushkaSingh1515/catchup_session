def check_length(ISBN: str) -> bool:
    ISBN = ISBN.replace("-", "").replace(" ", "")
    return len(ISBN) == 13 and ISBN.isdigit()

print(check_length("978-3-16-148410-0"))

def is_valid(ISBN: str) -> bool:
    if check_length(ISBN):
        ISBN = ISBN.replace("-", "").replace(" ", "")
        digits = list(map(int, ISBN))
        sum_of_dig = 0
        for i in range(13):
            if i % 2 == 0:
                sum_of_dig += digits[i]
            else:
                sum_of_dig += 3 * digits[i]
        return sum_of_dig % 10 == 0
    return False

print(is_valid("978-3-16-148410-0"))

