def is_valid_isbn_13(isbn : str) -> bool:
    isbn = isbn.replace("-", "").replace(" ", "")
    if len(isbn) != 13 or not isbn.isdigit():
        return False
    return check_sum_isbn(isbn) 
    
def check_sum_isbn(isbn : str)-> bool:
    # digits = list(map(int, isbn))
    # checksum = 0
    # for i in range(12):
    #     if i % 2 == 0:
    #         checksum += digits[i]
    #     else:
    #         checksum += 3 * digits[i]
    # check_digit = (10 - (checksum % 10)) % 10
    # return check_digit == digits[12]
    digits = [int(digit) for digit in isbn]
    checksum = sum(digits[i] if i % 2 == 0 else 3 * digits[i] for i in range(12))
    check_digit = (10 - (checksum % 10)) % 10
    return check_digit == digits[12]


Number = "978-3-16-148410-0"
if is_valid_isbn_13(Number):
    print(f"{Number} is a valid ISBN-13.")
else:
    print(f"{Number} is not a valid ISBN-13.")