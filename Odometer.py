def is_strictly_ascending(num):
    digits = list(str(num))
    return all(digits[i] < digits[i+1] for i in range(len(digits) - 1))
    
def next_number(num):
    while True:
        num += 1
        if is_strictly_ascending(num):
           return num

def next_strictly_ascending(number):
    number = int(number)
    return next_number(number)



print(next_strictly_ascending(657))
print(next_strictly_ascending(547))
print(next_strictly_ascending(563))
print(next_strictly_ascending(896))
print(next_strictly_ascending(123))
print(next_strictly_ascending(369))
