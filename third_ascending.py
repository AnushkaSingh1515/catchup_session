def is_strictly_ascending(num):
    digits = list(str(num))
    return all(digits[i] < digits[i+1] for i in range(len(digits) - 1))
    
def next_number(num):
    while True:
        num += 1
        if is_strictly_ascending(num):
           return num

def third_ascending(n):
    second_ascending = next_number(n)
    while True:
        second_ascending += 1
        if is_strictly_ascending(second_ascending):
            return second_ascending
        
print(third_ascending(336))
print(third_ascending(248))







