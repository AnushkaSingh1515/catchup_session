def check_boundaries(num: int) -> list[int]:
     no_of_dig = len(str(num))
     boundaries = []
     high = 0
     low = 0
     for i in range(9 - no_of_dig + 1,10):
         high = high * 10 + i
     for i in range(1, 1 + no_of_dig):
           low = low * 10 + i
     boundaries.append(low)
     boundaries.append(high)
     return boundaries
  
def is_strictly_ascending(num):
     digits = list(str(num))
     return all(digits[i] < digits[i+1] for i in range(len(digits) - 1))
  
def find_previous(num: int) -> int:
     boundaries = check_boundaries(num)
     for i in range(num-1, boundaries[0],-1):
          if(is_strictly_ascending(i) and '0' not in str(i)):
              return i
     return boundaries[1]
  

def find_difference(a:int,b:int) -> int:
    c = 0
    if(len(str(a)) != len(str(b))):
       return -1
    for i in range(a,b):
        if(is_strictly_ascending(i)):
            c += 1
    return c

print(find_difference(345,347))
print(find_difference(345,3475))

