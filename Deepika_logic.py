def get_limit(n: int) -> tuple[int, int]:
    DIGITS = "123456789"
    k = len(str(n))
    return int(DIGITS[:k]), int(DIGITS[-k:])

def is_ascending(n: int) -> bool:
    if n < 10:
        return True
    digits = str(n)
    for i in range(len(digits) - 1):
        if digits[i] >= digits[i + 1]:
            return False
    return True

def next_reading(n: int) -> int:
    start, limit = get_limit(n)
    if n == limit:
        return start
    n += 1
    while not is_ascending(n):
        n += 1
    return n

def previous_reading(n: int) -> int:
    start, limit = get_limit(n)
    if n == start:
        return limit
    n -= 1
    while not is_ascending(n):
        n -= 1
    return n

def kth_next_reading(k: int, n: int) -> int:
    for _ in range(k):
        n = next_reading(n)
    return n

def kth_previous_reading(k: int, n: int) -> int:
    for _ in range(k):
        n = previous_reading(n)
    return n

def distance(a: int, b: int) -> int:
    if len(str(a)) != len(str(b)):
        return -1
    dist = 0
    while a != b:
        dist += 1
        a = next_reading(a)
    return dist


print(next_reading(123))
print(next_reading(789))
print(previous_reading(138))
print(previous_reading(123))
print(kth_next_reading(3, 123))
print(kth_previous_reading(3, 138))
print(distance(345, 346))
